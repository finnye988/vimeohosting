from flask import Flask, render_template, request, redirect, url_for
from werkzeug.utils import secure_filename
import requests
from flask_uploads import UploadSet, configure_uploads, IMAGES, VIDEO  # Import VIDEO

# Replace with your Vimeo API credentials
CLIENT_ID = "YOUR_CLIENT_ID"
CLIENT_SECRET = "oxWp+TgjOJ3hEPcdvvtDCl8yhNt3InJuJr6uoxlmMDKx+LZIBd97D93x35RN7BlC2cXbxZNqDXtwL5TnuzHGmHrFSilLMAsO3WcNLV1yCTQlxFCPTMpc8bw7YI7Xuxsm"
ACCESS_TOKEN = "7e2a0a56781600d5a514f6b26643105e"

app = Flask(__name__)

# Configure Flask-Uploads
app.config['UPLOADED_VIDEO_DEST'] = 'uploads/'  # Set upload directory (adjust path)
videos = UploadSet('videos', extensions=VIDEO)  # Allow video uploads
configure_uploads(app, videos)

# Helper function to make API requests
def vimeo_api_call(url, method="GET", data=None):
  headers = {"Authorization": f"Bearer {ACCESS_TOKEN}"}
  response = requests.request(method, url, headers=headers, data=data)
  return response.json()

# Route for uploading videos
@app.route("/upload", methods=["GET", "POST"])
def upload():
  if request.method == "GET":
    return render_template("upload.html")  # Display upload form
  else:
    # Handle video upload logic
    video = request.files.get('video')
    if video and video.filename != '':
      filename = secure_filename(video.filename)
      videos.save(filename)  # Securely save the video
      # Simulate Vimeo upload (replace with actual API interaction)
      video_url = f"https://vimeo.com/{filename}"  # Placeholder URL
      return render_template("upload_success.html", video_url=video_url)
    else:
      error_message = "Please select a video file to upload."
      return render_template("upload.html", error_message=error_message)

# Route to display uploaded video
@app.route("/videos")
def show_videos():
  # Get uploaded video information (replace with logic to retrieve uploaded info, e.g., database lookup)
  video_data = vimeo_api_call(f"https://api.vimeo.com/videos/{video_url}")
  return render_template("show_video.html", video_data=video_data)

if __name__ == "__main__":
  app.run(debug=True)
